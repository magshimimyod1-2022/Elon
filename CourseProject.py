def first_print():
    """
    print the first thing in the game
    """
    HANGMAN_ASCII_ART = "Welcome to the game Hangman\n  _    _ \n | |  | |\n | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  \n |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ \n | |  | | (_| | | | | (_| | | | | | | (_| | | | |\n |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|\n                      __/ | \n                     |___/"
    MAX_TRIES = 6
    print(HANGMAN_ASCII_ART)
    print("you have %d tries" % MAX_TRIES)


def choose_word(file_path, index):
    """
    print the choosen word
    :param file_path: the path to the word file
    :param type:str
    :param index:the num of the plce of the wanted str
    :param type: int
    :return:the choosen word
    :rtype:str
    """
    file = open(file_path, 'r')
    file_str = file.read()
    file_list = file_str.split(" ")
    while index > len(file_list):
        index -= len(file_list)
    return file_list[index - 1]


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """check if the input is valid and if it is adds it to the old guesses but if not valid print the list
:param latter_gussed: the user input
:param type: str
:param old_letters_guessed: list of old guasess
:param type: list
:return: flag
:rtype:bool"""
    letter_guessed = letter_guessed.lower()
    if len(letter_guessed) > 1:
        flag = False
    elif letter_guessed < 'a' and letter_guessed < 'z':
        flag = False
    elif old_letters_guessed.count(letter_guessed) >= 1:
        flag = False
    else:
        flag = True
    old_letters_guessed.sort()
    if flag:
        old_letters_guessed.append(letter_guessed)
    else:
        print("X")
        print(' -> '.join(old_letters_guessed))
    return flag


def show_hidden_words(secret_word='saaaaaaa', old_latter_guessed=['a', 'b']):
    """
    return an str with the corrected latters shown and the others as lines
    :param secret_word: the word the user needs to guess
    :param type:str
    :param old_latter_guessed:a list of all the former guesses
    :param type: list
    :return: shown_secret_words, the word with only the corrected guessed latters(other are lines)
    :rtype:str
    """
    secret_word_length = len(secret_word)
    shown_secret_words = "_ " * secret_word_length
    secret_list = shown_secret_words.split()
    for i in old_latter_guessed:
        for j in range(len(secret_word)):
            if i == secret_word[j]:
                secret_list[j] = i
    shown_secret_words = ' '.join(secret_list)
    return shown_secret_words


def print_hangman(num_of_tries):
    HANGMAN_PHOTOS = {1: "    x-------x",
                      2: "    x-------x\n    |\n    |\n    |\n    |\n    |",
                      3: "    x-------x\n    |       |\n    |       0\n    |\n    |\n    |",
                      4: "    x-------x\n    |       |\n    |       0\n    |       |\n    |\n    |",
                      5: "    x-------x\n    |       |\n    |       0\n    |      /|\ \n    |\n    |",
                      6: "    x-------x\n    |       |\n    |       0\n    |      /|\ \n    |      /\n    |",
                      7: "    x-------x\n    |       |\n    |       0\n    |      /|\ \n    |      / \ \n    |"}
    print(HANGMAN_PHOTOS[num_of_tries])


def check_win(secret_word, old_letters_guessed):
    """
    check if the player won
    :param secret_word: aword the user supposed to guess
    :param type: str
    :param old_letters_guessed: list of all the letters he guessed
    :param type: list
    :return: check_won,true-the player guesed right false-he didnt
    :rtype:bool
    """
    guessed_right_counter = 0
    check_won = False
    len_secret_word = len(secret_word)
    for i in old_letters_guessed:
        for j in secret_word:
            if i == j:
                guessed_right_counter = guessed_right_counter + 1
    if guessed_right_counter == len_secret_word:
        check_won = True
    return check_won


def main():
    lives = 1
    first_print()
    file_path = input("enter the file route: ")
    index = int(input("enter a number: "))
    secret_word = choose_word(file_path, index)
    print("_ " * len(secret_word))
    old_guess = []
    while lives <= 6:
        valid_gues = False
        while not valid_gues:
            guessed_latter = input("enter your guessed latter: ")
            valid_gues = try_update_letter_guessed(guessed_latter, old_guess)
        if not guessed_latter in secret_word:
            print(":(")
            print_hangman(lives)
            lives += 1
        print(show_hidden_words(secret_word, old_guess))
        won = check_win(secret_word, old_guess)
        if won:
            print("WIN")
            break
    won = check_win(secret_word, old_guess)
    if not won:
        print("LOSE")


main()
