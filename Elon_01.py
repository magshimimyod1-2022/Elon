import requests


def find_file_name(html_list):
    """
    find the name of the file
    :param html_list: the list of all the thing input from the website
    :return: list of the names
    """
    list = []
    for file in html_list:
        for i in range(len(file)):
            if file[i:i + 4] == "file":
                list.append(file[i:i + 10])
    return list[2::2]
    # get twice the name of the same file and in the start have atext with "files"


def extract_password_from_site():
    """
    extract the password from the website:)
    """
    URL = "http://webisfun.cyber.org.il/nahman/files/"
    response = requests.get(URL)
    html = response.text
    html_list = html.split("\n")
    html_list = find_file_name(html_list)
    password = ""
    for file_name in html_list:
        URL += file_name
        response = requests.get(URL)
        html = response.text
        password += html[99]

        URL = "http://webisfun.cyber.org.il/nahman/files/"
    return password


# extract_from_password()


def selection_sort(count_list, str_list):
    """
    sort a word according to how manytime she comes up
    :param count_list: the list which represent how many time a certain word come up
    :param str_list: the list of the words
    :return: sorted b:)
    """
    # kredit to C class who talk about specific sorting
    for i in range(len(count_list)):
        mi = i

        for j in range(i + 1, len(count_list)):
            if count_list[j] < count_list[mi]:
                mi = j

        if i != mi:
            count_list[i], count_list[mi] = count_list[mi], count_list[i]
            str_list[i], str_list[mi] = str_list[mi], str_list[i]  # so the no double also change
    return str_list


def find_most_common_words(rout_file=r"top250.txt", len_sentance=6):
    with open(rout_file, 'r') as file:
        file_read = file.read()
    file_list = file_read.split(" ")
    count_file_list = []
    no_double_list = list(dict.fromkeys(file_list))
    # so in the counter it wont spam the same latters
    for word in no_double_list:
        count_file_list.append(file_list.count(word))
    no_double_list = selection_sort(count_file_list, no_double_list)
    no_double_list = no_double_list[::-1]
    # return smallest to biggest:)
    password = " ".join(no_double_list[:len_sentance:])
    return password


# find_most_common_words()

def main():
    print("Welcome to the password finder of Nahman!\n\nchoose an option:")
    print("1 - password for first website")
    print("2 - sentence for second website")
    choice = int(input("-> "))

    if choice == 1:
        print("please wait a moment...")
        print('password: ', extract_password_from_site())
    elif choice == 2:
        print('sentence: ', find_most_common_words(r'words.txt', 6))

if __name__ == '__main__':
    main()

