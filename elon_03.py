import socket
SERVER_IP="54.71.128.194"
SERVER_PORT=92
def main():
    with socket.socket() as listening_sock:
        listening_sock.bind(('',9090))
        while(1==1):
            #endless loop:)
            listening_sock.listen(1)
            client_soc, client_address=listening_sock.accept()
            with client_soc:
                client_msg=client_soc.recv(1024)
                client_msg=client_msg.decode()
                print(client_msg)
                with socket.socket() as sock:
                    server_address=(SERVER_IP,SERVER_PORT)
                    sock.connect(server_address)
                    sock.sendall(client_msg.encode())
                    server_msg=sock.recv(1024)
                    server_msg=server_msg.decode()
                    print(server_msg)
                    client_msg=client_msg.split("&")
                    country=client_msg[-1].lower()
                    print(country)
                    if country=="country:france":
                        server_msg='ERROR#"France is banned!"'
                        print(server_msg)
                    if server_msg[:11]=="SERVERERROR":
                        server_msg=server_msg.split("#")
                        server_msg[0]="ERROR"
                        server_msg="#".join(server_msg)
                    print(server_msg)
                    if server_msg[:5]!="ERROR":
                        server_msg=server_msg.split("&")
                        for place,str in enumerate(server_msg) :
                            if ":" not in str:
                                server_msg[place-1]=server_msg[place-1]+" and "+server_msg[place]
                                server_msg.pop(place)
                        server_msg="&".join(server_msg)
                        server_msg=server_msg.split("&directors")
                        server_msg[0]=server_msg[0][:-1]+' proxy"'
                        server_msg="&directors".join(server_msg)
                        print(server_msg)
                        server_msg=server_msg.split("jpg")
                        server_msg=".jpg".join(server_msg)
                    print(server_msg)
                    client_soc.sendall(server_msg.encode())
                    client_response=client_soc.recv(1024)
                    client_response=client_response.decode()
                    print(client_response)
                        
if __name__=="__main__":
    main()